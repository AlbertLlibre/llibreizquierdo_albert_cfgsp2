<?php

require_once(__DIR__.'/../app/inc/constants.php');
require_once(__DIR__.'/../app/controller/IndexController.php');
 
$cnt = new IndexController();
$fs = $cnt->deatilsPersona($_GET['index']);

 
?><html>
<head>
 <title>Update Empleado</title>
</head>
<body>
 <h1>Update Empleado</h1>
 <form id="thform" method="post" action="/forms/update.php">
 <dl>
    <dt><label for="addt-name">Nom</label></dt>
    <dd><input type="text" id="addt-name" name="n" tabindex="1" value="<?=$fs->getName()?>"/></dd>
    <dt><label for="addt-surname">Cognom</label></dt>
    <dd><input type="text" id="addt-surname" name="s" tabindex="2" value="<?=$fs->getSurname()?>"/></dd>
    <dt><label for="addt-birthdays">Data</label></dt>
    <dd><input type="text" id="addt-birthday" name="b" tabindex="3" value="<?=$fs->getBirthday()?>"/></dd>
    <dt><label for="addt-ap">Posicio actual</label></dt>
    <dd><input type="text" id="addt-ap" name="ac" tabindex="4" value="<?=$fs->getActual_position()?>"/></dd>
    <dt><label for="addt-photo">Imatge</label></dt>
    <dd><input type="text" id="addt-image" name="p" tabindex="5" value="<?=$fs->getImage()?>"/></dd>
    <dt><label for="addt-salary">Sou</label></dt>
    <dd><input type="text" id="addt-salary" name="sa" tabindex="6" value="<?=$fs->getSalary()?>"/></dd>
    <dt>&nbsp;</dt>
    <dt><input type="hidden" name="i" value="<?=$fs->getId()?>"/></dt>
    <dd><input type="submit" value="add" name="sub/"></dd>

 </dl>
 </form>
 <a href="/">Inici</a>
</body>
</html>

