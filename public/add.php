<html>
  <head>
    <title>Afegir Empleats</title>
  </head>
  <body>
    <h1>Afegir Empleats</h1>
    <form id="thform" method="post" action="/forms/add.php" enctype="multipart/form-data">
    <dl>
        <dt><label for="addt-name">Name</label></dt>
        <dd><input type="text" id="addt-name" name="n" tabindex="1"/></dd>
        <dt><label for="addt-surname">Surname</label></dt>
        <dd><input type="text" id="addt-surname" name="s" tabindex="2"/></dd>
        <dt><label for="addt-birthday">birthday</label></dt>
        <dd><input type="text" id="addt-birthday" name="b" tabindex="3"/></dd>
        <dt><label for="addt-actual">Actual Position</label></dt>
        <dd><input type="text" id="addt-actual" name="ac" tabindex="4"/></dd>
        <dt><label for="addt-salary">Salary</label></dt>
        <dd><input type="text" id="addt-salary" name="sa" tabindex="5" value="" /></dd>
        <dt><label for="addt-image">Imagen</Imagenlabel></dt>
        <dd><input type="file" id="addt-image" name="img" tabindex="6"/></dd>

        <dt>&nbsp;</dt>
        <dd><input type="submit" value="Add" name="sub"/></dd>
    </dl>
    </form>
    <a href="/">Inici</a>
  </body>
</html>