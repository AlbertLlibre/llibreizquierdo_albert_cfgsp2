<?php
require_once(__DIR__.'/../app/inc/constants.php');
require_once(__DIR__.'/../app/controller/IndexController.php');

$cnt = new IndexController();
$fs = $cnt->listPersonas();

?>
<html>
  <head>
    <title>Albert Llibre</title>
  </head>
  <body>
    <h1>Projecte 2: L’agenda de l’empresa en funcionament</h1>
    <a href="/add.php">Afegir empleat: </a>
      <ul>
        <?php foreach($fs as $f){ ?>
            <li>
              <a href="/details.php?index=<?=$f->getId()?>">
                <?=$f->getName()?> <?=$f->getSurname()?>
              </a>
              <a href="/update.php?index=<?=$f->getId()?>">Update</a>
              <a href="/delete.php?index=<?=$f->getId()?>">Delete</a>
            </li>
        <?php } ?>
    </ul>
  </body>
</html>