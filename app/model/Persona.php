<?php

class Persona{

    private $_name;
    private $_surname;
    private $_birthday;
    private $_actual_position;
    private $_salary;
    private $_image;
    private $_id;

    public function __construct($name, $surname, $birthday, $actual_position, $salary, $image, $id){
        $this->setName($name);
        $this->setSurname($surname);
        $this->setBirthday($birthday);
        $this->setActual_position($actual_position);
        $this->setSalary($salary);
        $this->setImage($image);
        $this->setId($id);
    }

    public function getId(){
        return $this->_id;
    }

    public function getName(){
        return $this->_name;
    }

    public function getSurname(){
        return $this->_surname;
    }

    public function getBirthday(){
        return $this->_birthday;
    }

    public function getActual_position(){
        return $this->_actual_position;
    }
    public function getImage(){
        return $this->_image;
    }

    public function getSalary(){
        return $this->_salary;
    }


    public function setId($v){
        $this->_id = $v;
    }

    public function setName($v){
        $this->_name = $v;
    }

    public function setSurname($v){
        $this->_surname = $v;
    }

    public function setBirthday($v){
        $this->_birthday = $v;
    }

    public function setActual_position($v){
        $this->_actual_position = $v;
    }

    public function setImage($v){
        $this->_image = $v;
    }

    public function setSalary($v){
        $this->_salary = $v;
    }
    

}
