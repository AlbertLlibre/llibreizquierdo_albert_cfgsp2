<?php

require_once(__DIR__.'/../Persona.php');

class PersonasDb{

    private $_conn;

    public function listPersonas(){
        //OPEN CONNECTION TO DATABASE
        $this->openConnection();
         
        //PREPARE STATEMENT WITH NO PARAMETERS AT THE MOMENT
        $query = "SELECT * FROM persona";
        $stmt = $this->_conn->prepare($query);
        
        //EXECUTE QUERY
        $stmt->execute();
        $res = $stmt->get_result();
        
        //RETRIEVE RESULTS AND BUILD RETURN ARRAY
        $fabs = array();
        while ($fab = $res->fetch_assoc() ) {
          array_push($fabs, new Persona($fab['name'], $fab['surname'], $fab['birthday'], $fab['actual_position'], $fab['salary'], $fab['image'], $fab['idt']));
        }
        return $fabs;
      }
      
      public function getPersona($tid){
        //OPEN CONNECTION TO DATABASE
        $this->openConnection();

        //PREPARE STATEMENT WITH ONE PARAMETER
        $query = "SELECT * FROM persona WHERE idt = ?";
        $stmt = $this->_conn->prepare($query);

        //DEFINE PAFAMETER
        $stmt->bind_param("i", $index);
        $index = $tid;
    
        //EXECUTE QUERY
        $stmt->execute();
        $res = $stmt->get_result();
    
        //RETRIEVE RESULT AND BUILD RETURN OBJECT
        $fabres = $res->fetch_assoc();
        return new Persona($fabres['name'], $fabres['surname'], $fabres['birthday'], $fabres['actual_position'], $fabres['salary'], $fabres['image'], $fabres['idt']);
      }

      public function insertPersona($name, $surname, $birthday, $actual_position, $salary, $image){
        //OPEN CONNECTION TO DATABASE
        $this->openConnection();

        //PREPARE STATEMENT WITH ONE PARAMETER
        $query = "INSERT INTO persona (name, surname, birthday, actual_position, salary, image) VALUES (?, ?, ?, ?, ?, ?)";
        $stmt = $this->_conn->prepare($query);

        //DEFINE PAFAMETER
        $stmt->bind_param("ssssss", $n, $s, $b, $a, $sa, $img);
        $n = $name;
        $s = $surname;
        $b = $birthday;
        $a = $actual_position;
        $sa = $salary;
        $img = $image;
    
        //EXECUTE QUERY
        $stmt->execute();

        //echo('ID insert');
        //var_dump($stmt->insert_id);
    
        //RETRIEVE RESULT AND BUILD RETURN OBJECT
        return $this->getPersona($stmt->insert_id);
      }

        public function updateImage($id, $image){
        //OPEN CONNECTION TO DATABASE
        $this->openConnection();

         //PREPARE STATEMENT WITH ALL THE AVAILABLE PARAMETERS
         $query = "UPDATE persona SET image = ? WHERE idt = ? ";
         $stmt = $this->_conn->prepare($query);

          //DEFINE PAFAMETER
        $stmt->bind_param("si", $img, $tid);
        $tid = $id;
        $img = $image;

         //EXECUTE QUERY
         $stmt->execute();
    
         //RETRIEVE RESULT AND BUILD RETURN OBJECT
         return $this->getPersona($id);
        }
         public function updateRegPersona($name, $surname, $birthday, $actual_position, $salary, $image, $id){
          //OPEN CONNECTION TO DATABASE
          $this->openConnection();
  
          //PREPARE STATEMENT WITH ALL THE AVAILABLE PARAMETERS
          $query = "UPDATE persona SET name = ?, surname = ?, birthday = ?, actual_position = ?, salary = ?, image = ? WHERE idt = ? ";
          $stmt = $this->_conn->prepare($query);

          $stmt->bind_param("ssssssi", $n, $s, $b, $a, $sa, $img, $i);
          $n = $name;
          $s = $surname;
          $b = $birthday;
          $a = $actual_position;
          $sa = $salary;
          $img = $image;
          $i = $id;

          $stmt->execute();
         

          //RETRIEVE RESULT AND BUILD RETURN OBJECT
         return $this->getPersona($id);
         }

    public function removePersona($id){
        //OPEN CONNECTION TO DATABASE
        $this->openConnection();

        //PREPARE STATEMENT WITH ONE PARAMETER
        $query = "DELETE FROM persona WHERE idt = ?";
        $stmt = $this->_conn->prepare($query);

        //DEFINE PAFAMETER
        $stmt->bind_param("i", $index);
        $index = $id;
    
        //EXECUTE QUERY
        $stmt->execute();
        return true;
    }


    private function openConnection(){
      if($this->_conn == NULL){
        $this->_conn = mysqli_connect(DB_HOST, DB_USER, DB_PWD, DB_DB);    
      }
  }

  
}

