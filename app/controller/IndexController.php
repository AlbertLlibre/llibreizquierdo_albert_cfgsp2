<?php
require_once(__DIR__.'/../model/Persona.php');
require_once(__DIR__.'/../model/db/PersonasDb.php');

class IndexController{

    public function listPersonas(){
        $db = new PersonasDb();
        $fabs = $db->listPersonas();
        
        return $fabs;
    }

    public function deatilsPersona($pos = 0){
       $db = new PersonasDb();
       return $db->getPersona($pos);
    }

    public function createPersona($name, $surname, $birthday, $actual_position, $salary, $image){
        $db = new PersonasDb();
        $persona = $db->insertPersona($name, $surname, $birthday, $actual_position, $salary, $image);
        $ext = substr($_FILES['img']['name'],strpos($_FILES['img']['name'],'.'));
        $name = $persona->getId().'-main'.$ext;
        move_uploaded_file($_FILES['img']['tmp_name'], IMG_PATH.$name);
        return $db->updateImage($persona->getId(), $name);
     }


     public function updatePersona($name, $surname, $birthday, $actual_position, $salary, $image, $tid){
      $db = new PersonasDb();
      $persona -> updatePersona($name, $surname, $birthday, $actual_position, $salary, $image, $tid);
      $ext = substr($_FILES['img']['name'],strpos($_FILES['img']['name'],'.'));
      $name = $persona->getId().'-main'.$ext;
      return $db->updateImage($persona->getId(), $name);
   }


     public function deletePersona($tid){
        $db = new PersonasDb();
        $fab = $db->getPersona($tid);

        $db->removePersona($tid);

        return $fab;
     }

}
