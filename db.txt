create table persona (

    idt INT PRIMARY KEY AUTO_INCREMENT,
    Name varchar(25), surname varchar(50),
    birthday date, actual_position VARCHAR(50),
    salary INT, image VARCHAR(255)
);

insert into persona (Name,surname,birthday,actual_position,salary, image)
values('Albert','Llibre','2000-02-08','CEO',2000,Null);